# Curso-Django-Platzi

Curso inicial de Django en Platzi.

## 1 - Introducción

* 01 - Introducción al curso
* 02 - Historia de web development
* 03 - Preparación del entorno de trabajo  en Mac
* 04 - Preparación del entorno de trabajo en Windows y Linux (Doc.)
* 05 - Creación del proyecto Platzigram  Tu primer Hola, mundo! en Django

## 2 - Vistas

* 01 - El objeto Request
* 02 - Solución al reto - Pasando argumentos en la URL
* 03 - Creación de la primera app
* 04 - Introducción al template system
* 05 - Patrones de diseño y Django

## 3 - Models

* 01 - La M en el MTV
* 02 - El ORM de Django
* 03 - Glosario (Doc.)
* 04 - Extendiendo el modelo de usuario
* 05 - Implementación del modelo de usuarios de Platzigram
* 06 - Explorando el dashboard de administración
* 07 - Dashboard de Administración
* 08 - Creación del modelo de posts

## 4 - Templates, auth y middlewares

* 01 - Templates y archivos estáticos
* 02 - Login
* 03 - Logout
* 04 - Signup
* 05 - Middlewares

## 5 - Forms

* 01 - Formularios en Django
* 02 - Mostrando el form en el template
* 03 - Model forms
* 04 - Validación de formularios

## 6 - Class-based views

* 01 - Class-based views
* 02 - Protegiendo la vista de perfil, Detail View y List View
* 03 - CreateView, FormView y UpdateView
* 04 - Generic auth views

## 7 - Deployment

* 01 - Arquirectura  Conceptos  Componentes (Doc.)
* 02 - ¿Cómo conectar Django a una base de datos? (Doc.)
* 03 - Configurar el servidor (Doc.)
* 04 - Preparación del VPS (en AWS) (Doc.)
* 05 - Conclusiones del curso

## 8 - Bonus

* 01 - ¿Cómo usar los templates en Django?
