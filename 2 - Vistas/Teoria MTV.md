## Model Template View o MTV

* Model : Define la estructura de los datos.
* Template : Logica de presentacion de datos.
* View : Encargado de traer los datosy pasarlos al template.

Si hacemos una analogia con MVC. El controller es la vista y las urls que manejan esa logica, mientras que la parte del view seria el template.
